using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GridMapCreator : EditorWindow
{
    Vector2 offset;
    Vector2 drag;
    Vector2 nodePos;

    List<List<Node>> nodes;
    List<List<PartScript>> parts;

    GUIStyle empty;
    GUIStyle currentStyle;

    StyleManager styleManager;

    Rect MenuBar;

    bool isErasing;

    GameObject Map;

    [MenuItem("Window/Grid Map Creator")]
    private static void OpenWindow()
    {
        GridMapCreator window = GetWindow<GridMapCreator>();

        window.titleContent = new GUIContent("Grid Map Creator");

    }

    private void OnEnable()
    {
        SetupStyles();
        SetUpNodesAndParts();
        SetupMap();
    }

    private void SetupMap()
    {
        try
        {
            Map = GameObject.FindGameObjectWithTag("Map");
            RestoreMap(Map);
        }
        catch(Exception e)
        {
            if (Map == null)
            {
                Map = new GameObject("Map");
                Map.transform.position = new Vector2(-9.5f, -4.5f);
                Map.tag = "Map";
            }
        }
    }

    private void RestoreMap(GameObject map)
    {
        if(map.transform.childCount > 0)
        {
            for (int i = 0; i < map.transform.childCount; i++)
            {
                int childRow = map.transform.GetChild(i).GetComponent<PartScript>().Row;
                int childCol = map.transform.GetChild(i).GetComponent<PartScript>().Column;

                GUIStyle childStyle = map.transform.GetChild(i).GetComponent<PartScript>().style;
                nodes[childRow][childCol].SetStyle(childStyle);
                parts[childRow][childCol] = map.transform.GetChild(i).GetComponent<PartScript>();
                parts[childRow][childCol].Part = map.transform.GetChild(i).gameObject;
                parts[childRow][childCol].name = map.transform.GetChild(i).name;
                parts[childRow][childCol].Row = childRow;
                parts[childRow][childCol].Column= childCol;
            }
        }
    }

    private void SetupStyles()
    {
        try 
        { 
            styleManager = GameObject.FindGameObjectWithTag("StyleManager").GetComponent<StyleManager>();
            for (int i = 0; i < styleManager.buttonStyles.Length; i++)
            {
                styleManager.buttonStyles[i].NodeStyle = new GUIStyle();
                styleManager.buttonStyles[i].NodeStyle.normal.background = styleManager.buttonStyles[i].Icon;
            }
        }
        catch (Exception e) 
        {
        }

        empty = styleManager.buttonStyles[0].NodeStyle;
        currentStyle = styleManager.buttonStyles[1].NodeStyle;
    }

    private void SetUpNodesAndParts()
    {
        nodes = new List<List<Node>>();
        parts = new List<List<PartScript>>();

        for (int i = 0; i < 20; i++)
        {
            nodes.Add(new List<Node>());
            parts.Add(new List<PartScript>());
            for (int j = 0; j < 10; j++)
            {
                nodePos.Set(i * 30, j * 30);
                nodes[i].Add(new Node(nodePos, 30, 30, empty));
                parts[i].Add(null);
            }
        }
    }

    private void OnGUI()
    {
        DrawGrid();
        DrawNodes();
        DrawMenuBar();
        ProcessNodes(Event.current);
        ProcessGrid(Event.current);
        if (GUI.changed)
        {
            Repaint();
        }
    }

    private void DrawMenuBar()
    {
        MenuBar = new Rect(0, 0, position.width, 40);
        GUILayout.BeginArea(MenuBar, EditorStyles.toolbar);

        GUILayout.BeginHorizontal();

        for (int i = 0; i < styleManager.buttonStyles.Length; i++)
        {
            if (GUILayout.Toggle((currentStyle == styleManager.buttonStyles[i].NodeStyle),
                new GUIContent(styleManager.buttonStyles[i].ButtonText), EditorStyles.toolbarButton, GUILayout.Width(75)))
            {
                currentStyle = styleManager.buttonStyles[i].NodeStyle;
            }
        }
        if(GUILayout.Button(new GUIContent("Save"), GUILayout.Width(75)))
        {
            EditorSceneManager.SaveScene(SceneManager.GetActiveScene());
        }
        
        GUILayout.EndHorizontal();

        GUILayout.EndArea();
    }

    private void ProcessNodes(Event e)
    {
        int row = (int)((e.mousePosition.x - offset.x) / 30);
        int col = (int)((e.mousePosition.y - offset.y) / 30);

        if ((e.mousePosition.x - offset.x) < 0 || (e.mousePosition.x - offset.x) > 600 || (e.mousePosition.y - offset.y) < 0 ||
            (e.mousePosition.y - offset.y) > 300)
            return;

        if(e.type == EventType.MouseDown)
        {
            if(nodes[row][col].style.normal.background.name == "Empty")
            {
                isErasing = false;
            }
            else
            {
                isErasing = true;
            }
            PaintNodes(row, col);
        }

        if(e.type == EventType.MouseDrag)
        {
            PaintNodes(row, col);
            e.Use();
        }
    }

    private void PaintNodes(int row, int col)
    {
        int correctCol = 9 - col > 0 ? 9 - col : (9 - col) * -1;

        if (isErasing)
        {
            if (parts[row][col] != null)
            {
                nodes[row][col].SetStyle(empty);

                DestroyImmediate(parts[row][col].gameObject);
                if (currentStyle.Equals(empty) ||
                    parts[row][col].style.Equals(currentStyle))
                {
                    parts[row][col] = null;
                }
                else
                {
                    nodes[row][col].SetStyle(currentStyle);

                    GameObject obj = Instantiate(Resources.Load("MapParts/" + currentStyle.normal.background.name)) as GameObject;
                    obj.name = currentStyle.normal.background.name;
                    obj.transform.position = new Vector2(row, correctCol);
                    obj.transform.parent = Map.transform;

                    parts[row][col] = obj.GetComponent<PartScript>();
                    parts[row][col].Part = obj;
                    parts[row][col].name = obj.name;
                    parts[row][col].Row = row;
                    parts[row][col].Column = col;
                    parts[row][col].style = currentStyle;
                }

            }
        }
        else
        {
            if(parts[row][col] == null)
            {
                nodes[row][col].SetStyle(currentStyle);

                GameObject obj = Instantiate(Resources.Load("MapParts/" + currentStyle.normal.background.name)) as GameObject;
                obj.name = currentStyle.normal.background.name;
                obj.transform.position = new Vector2(row, correctCol);
                obj.transform.parent = Map.transform;

                parts[row][col] = obj.GetComponent<PartScript>();
                parts[row][col].Part = obj;
                parts[row][col].name = obj.name;
                parts[row][col].Row = row;
                parts[row][col].Column = col;
                parts[row][col].style = currentStyle;

            }
        }
        GUI.changed = true;
    }

    private void DrawNodes()
    {
        for (int i = 0; i < 20; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                nodes[i][j].Draw();
            }
        }
    }

    private void ProcessGrid(Event e)
    {
        drag = Vector2.zero;
        switch (e.type)
        {
            case EventType.MouseDrag:
                if(e.button == 0)
                {
                    OnMouseDrag(e.delta);
                }
                break;
        }
    }

    private void OnMouseDrag(Vector2 delta)
    {
        drag = delta;

        for (int i = 0; i < 20; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                nodes[i][j].Drag(delta);
            }
        }

        GUI.changed = true;

    }

    private void DrawGrid()
    {
        int widthDivider = Mathf.CeilToInt(position.width / 20);
        int heightDivider = Mathf.CeilToInt(position.height / 20);
        Handles.BeginGUI();
        Handles.color = new Color(0.5f, 0.5f, 0.5f, 0.2f);

        offset += drag;
        Vector3 newOffset = new Vector3(offset.x % 20, offset.y % 20, 0);

        for (int i = 0; i < widthDivider; i++)
        {
            Handles.DrawLine(new Vector3(20 * i, -20, 0) + newOffset,
                            new Vector3(20 * i, position.height, 0) + newOffset);
        }
        for (int i = 0; i < heightDivider; i++)
        {

            Handles.DrawLine(new Vector3(-20, 20 * i, 0) + newOffset,
                            new Vector3(position.height, 20 * i, 0) + newOffset);
        }
        Handles.color = Color.white;
        Handles.EndGUI();
    }
}
