using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartScript : MonoBehaviour
{
    public int Row, Column;
    public string PartName = "Empty";

    public GameObject Part;
    public GUIStyle style;
}
