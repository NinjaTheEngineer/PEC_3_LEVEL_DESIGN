using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private bool m_ReadyForInput;
    public int numberOfLevels = 10;
    public Player m_Player;
    public GameObject m_ButtonNextLevel;
    public GameObject m_PanelLevelCompleted;
    private void Start()
    {
        m_ButtonNextLevel.SetActive(false);
        m_PanelLevelCompleted.SetActive(false);
        if (m_Player == null)
            m_Player = FindObjectOfType<Player>();
    }

    private void Update()
    {
        Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        moveInput.Normalize();

        if(moveInput.sqrMagnitude > 0.5f)
        {
            if (m_ReadyForInput)
            {
                m_ReadyForInput = false;
                m_Player.Move(moveInput);
                m_ButtonNextLevel.SetActive(IsLevelCompleted());
                m_PanelLevelCompleted.SetActive(IsLevelCompleted());
            }
        }
        else
        {
            m_ReadyForInput = true;
        }
    }

    private bool IsLevelCompleted()
    {
        Box[] boxes = FindObjectsOfType<Box>();
        foreach(var box in boxes)
        {
            if (!box.m_OnCross) return false;
        }
        return true;
    }

    public void NextLevel()
    {
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        if (nextSceneIndex >= numberOfLevels)
            Application.Quit();
        else
            SceneManager.LoadScene(nextSceneIndex);
    }

    public void ResetLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
